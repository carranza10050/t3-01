package com.example.t3_01;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.service.autofill.CustomDescription;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.example.t3_01.Adapter.NameAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    ArrayList<Conctacto> listCont;
    RecyclerView recyclerContacto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        listCont=new ArrayList<>();
        recyclerContacto=(RecyclerView) findViewById(R.id.rcv);
        recyclerContacto.setLayoutManager(new LinearLayoutManager(this));

        llenarDatos();



        NameAdapter adapter = new NameAdapter(listCont);
        recyclerContacto.setAdapter(adapter);



    }

    private void llenarDatos() {
        listCont.add(new Conctacto("lUIS CARRANZA","930458523"));
        listCont.add(new Conctacto("lUIS SALDAÑA","960458522"));
        listCont.add(new Conctacto("MESSI LIONEL","910458624"));
        listCont.add(new Conctacto("PEDRO VASQUEZ","980448521"));
        listCont.add(new Conctacto("RONAL CHUQUI","96045520"));
        listCont.add(new Conctacto("ANUEL LIONEL","930458529"));
        listCont.add(new Conctacto("MARIO VASQUEZ","940458526"));
        listCont.add(new Conctacto("MAXI LIONEL","999458528"));
        listCont.add(new Conctacto("GODZE VASQUEZ","530458527"));
        listCont.add(new Conctacto("RICKY SALDAÑA","960458511"));
        listCont.add(new Conctacto("MARTIN VASQUEZ","980458599"));

    }
}