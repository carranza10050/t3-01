package com.example.t3_01.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.t3_01.Conctacto;
import com.example.t3_01.R;

import java.util.ArrayList;
import java.util.List;

public class NameAdapter extends RecyclerView.Adapter<NameAdapter.NameViewHolder>{

    ArrayList<Conctacto> cont;
    public NameAdapter(ArrayList<Conctacto> cont) {
        this.cont = cont;
    }




    @Override
    public NameViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_name,null,false);
        return new NameViewHolder(view);
    }

    @Override
    public void onBindViewHolder( NameViewHolder holder, int position) {
       holder.nnombre.setText(cont.get(position).getNombre());
        holder.nnumero.setText(cont.get(position).getNumero());

        Button btl = holder.itemView.findViewById(R.id.button);
        btl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(holder.itemView.getContext(),"LLAMANDO",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", String.valueOf(holder.nnumero),null));

            }

        });
    }

    @Override
    public int getItemCount() {
        return cont.size();
    }

    public class NameViewHolder extends RecyclerView.ViewHolder{
        TextView nnombre,nnumero;
        public NameViewHolder( View itemView) {
            super(itemView);
            nnombre=(TextView) itemView.findViewById(R.id.idnombre);
            nnumero=(TextView) itemView.findViewById(R.id.idapellido);

        }
    }
}
